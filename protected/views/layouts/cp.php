<!DOCTYPE html>
<html lang="en">

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

		<?php Yii::app()->bootstrap->registerCss(); ?>
		<?php Yii::app()->bootstrap->registerYiiCss(); ?>
		
		<link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/favicon.ico" type="image/x-icon" />
		<title><?php echo CHtml::encode(Yii::app()->name . ' - ' . $this->pageTitle); ?></title>
	</head>
	
	<body>
	
			<!-- Отладчик EMAIL -->
			<?php $this->widget('application.extensions.email.Debug'); ?>
		
			<?php 
						
			$this->widget('bootstrap.widgets.BootNavbar',array(
				'fixed' => false,
				'brand' => Yii::app()->name,
				'brandUrl' => '/',
				'items' => array(
					array(
						'class' => 'bootstrap.widgets.BootMenu',		      
						'items'=>array(

							array('label'=>'Обновить индекс', 
									'url' => array('/projects/project/refreshFileIndex'),
									'linkOptions' => array('confirm' => 'Точно?')),

							array(
								'label' => 'Сервис', 
								'items' => array(
									array('label' => 'Резервные копии', 'url' => array('/backuper/backup/admin'), 'visible' => Yii::app()->user->checkAccess('backuper/project/admin')),
									array('label' => 'Gii', 'url' => array('/gii'), 'visible' => Yii::app()->user->checkAccess('assist'), 'linkOptions' => array('target' => '_blanc')),
									)),


							),
						),
					),
			)); ?>


		<div class='container-fluid'>    
			
			<div class='row-fluid'>

				<!-- ЛЕВОЕ МЕНЮ -->
				<div class="span2">
						<div class='well' style='padding: 10px 0;'>
							<?php 
							$this->widget('bootstrap.widgets.BootMenu', array(
									'type' => 'list',
									'stacked' => 'true',
									'items' => array(
										array('label' => 'Меню'),
										array('label' => 'Проекты', 'url' => array('/projects/project/admin'), 'visible' => Yii::app()->user->checkAccess('backuper/project/admin')),
										array('label' => 'Файлы', 'url' => array('/projects/file/admin'), 'visible' => Yii::app()->user->checkAccess('backuper/project/admin')),

										))); 
							
							?>
						</div>
				</div><!-- span2 -->
		
				<!-- ОСНОВНАЯ ОБЛАСТЬ -->  
				<div class="span8">    
					<?php if(isset($this->breadcrumbs)):?>
						<?php $this->widget('bootstrap.widgets.BootBreadcrumbs', array(
							'links' => $this->breadcrumbs + array($this->pageTitle),
							'homeLink' =>CHtml::link('Главная', array('/')),
						)); ?><!-- breadcrumbs -->
					<?php endif?>
					<?php if ($this->showTitle): ?><h1 style='margin-bottom:10px;'><?php echo $this->pageTitle; ?></h1><?php endif ?>
					
					<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
					<?php echo $content; ?>
				</div><!-- span9 -->
				
				<!-- ПРАВОЕ МЕНЮ -->
				<div class="span2">
					<?php if (!$this->isMenuNull): ?>
						<div class='well' style='padding: 10px 0;'>
							<?php   	
								$this->widget('bootstrap.widgets.BootMenu', array(
									'type' => 'list',
									'stacked' => true,
									'items' => array_merge(array(array('label' => 'Опции')), $this->menu),
								));        		
							?>
						</div>
					<?php endif; ?>
				</div><!-- span2 -->
			
			</div><!-- row -->
			
			
			<hr clear='all'>
			<div class='row-fluid'>
				<div class='span2'>&nbsp;</div>
				<div class='span8 offset2' style='text-align: center;'>  
					<footer>
						<p>© Deka-Web <?php echo date('Y') ?>.</p>
					</footer>
				</div>
			</div><!-- row -->  
		</div>
		
	</body>
</html>
