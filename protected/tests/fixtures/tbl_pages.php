<?php return array(
	'first' => array(
		'id' => 1, 
		'url' => 'first', 
		'title' => 'First',
		'flg_public' => 1),

		'first/first' => array(
			'id' => 4, 
			'id_parent' => 1, 
			'url' => 'first',
			'title' => 'FirstSecond',
			'flg_public' => 0,
			'flg_folder' => 1),

			'first/first/first' => array(
				'id' => 5, 
				'id_parent' => 4, 
				'url' => 'first',
				'title' => 'FirstSecondThird',
				'flg_public' => 1),

		'first/second' => array(
			'id' => 2, 
			'id_parent' => 1, 
			'url' => 'second',
			'title' => 'FirstSecond',
			'flg_public' => 0,
			'flg_folder' => 1),

			'first/second/third' => array(
				'id' => 3, 
				'id_parent' => 2, 
				'url' => 'third',
				'title' => 'FirstSecondThird',
				'flg_public' => 1),
	); 

?>
