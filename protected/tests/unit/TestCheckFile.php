<?php 

Yii::import('application.modules.projects.models.*');

class TestCheckFile extends DTestCase
{

	//----------------------------------------------------------------------------  
	public function testParseVersion()
	//----------------------------------------------------------------------------  
	{
		$modProject = new Project;

		$modCheckFile = new CheckFile;

		$modCheckFile->version = '1.a.2.3';

		$stt = $modCheckFile->getParsedVersion();

		$this->assertEqual($stt['a'], 1);
		$this->assertEqual($stt['b'], 2);
		$this->assertEqual($stt['c'], 3);
		$this->assertEqual($stt['d'], '');
		$this->assertEqual($stt['pb'], 'a');
		$this->assertEqual($stt['pc'], '');		
	}


	//----------------------------------------------------------------------------  
	public function testCheckCompatibility()
	//----------------------------------------------------------------------------  
	{
		$modCheckFile = new CheckFile;

		// Проверки A, B, C
		$this->assertTrue($this->checkCompatibility('1.1.1', '1.1'));		
		$this->assertTrue($this->checkCompatibility('1.1.1', '1.1.1'));
		$this->assertTrue($this->checkCompatibility('1.1.2', '1.1.1'));

		$this->assertFalse($this->checkCompatibility('1.1.1', '1.1.2'));
		$this->assertFalse($this->checkCompatibility('1.2.1', '1.1.1'));
		$this->assertFalse($this->checkCompatibility('2.1.1', '1.1.1'));

		// Проверки PA
		$this->assertTrue($this->checkCompatibility('1.a.1.2', '1.a.1.2'));	
		$this->assertFalse($this->checkCompatibility('1.a.1.2', '1.1.2'));	
		$this->assertFalse($this->checkCompatibility('1.1.2', '1.a.1.2'));	

		// Проверки PB
		$this->assertTrue($this->checkCompatibility('1.1.a.2', '1.1'));		
		$this->assertTrue($this->checkCompatibility('1.1.a.2', '1.1.2'));	

		$this->assertFalse($this->checkCompatibility('1.1.a.2', '1.1.3'));	
		$this->assertFalse($this->checkCompatibility('1.1.2', '1.1.a'));		
		$this->assertFalse($this->checkCompatibility('1.1.a.2', '1.1.a.3'));	

		// Дополнительные проверки
		$this->assertTrue($this->checkCompatibility('1.1.2', '1.1.1'));		
		$this->assertFalse($this->checkCompatibility('1.1.1', '1.1.2'));		

		$this->assertTrue($this->checkCompatibility('1.1.a.1', '1.1'));		
		$this->assertTrue($this->checkCompatibility('1.1.a.1', '1.1.a'));

		$this->assertTrue($this->checkCompatibility('1.1.1', '1.1'));


		$this->assertTrue($this->checkCompatibility('1.1.a.3', '1.1.3'));		
		$this->assertFalse($this->checkCompatibility('1.1.a.3', '1.1.4'));		

	}	

	public function testCanUpdate()
	{
		$modCheckFile = new CheckFile;
		$modCheckFile->version = '1.1.2';

		// Можно обновиться
		$this->assertTrue($modCheckFile->getCanUpdateTo('1.1.3'));		
		$this->assertTrue($modCheckFile->getCanUpdateTo('1.1.2.1'));		
		$this->assertTrue($modCheckFile->getCanUpdateTo('1.2.1'));		

		$this->assertFalse($modCheckFile->getCanUpdateTo('0.1.3'));		
		$this->assertFalse($modCheckFile->getCanUpdateTo('1.1.1.1'));		

		$this->assertFalse($modCheckFile->getCanUpdateTo('1.1.1'));		
		$this->assertFalse($modCheckFile->getCanUpdateTo('2.1.1'));		

		$this->assertFalse($modCheckFile->getCanUpdateTo('1.a.1.3'));		
		$this->assertFalse($modCheckFile->getCanUpdateTo('1.1.a.3'));		

		$modCheckFile = new CheckFile;
		$modCheckFile->version = '1.a.1.2';
		$this->assertFalse($modCheckFile->getCanUpdateTo('1.1.3'));		

		$modCheckFile = new CheckFile;
		$modCheckFile->version = '1.1.a.2';
		$this->assertFalse($modCheckFile->getCanUpdateTo('1.1.3'));		

	}

	//----------------------------------------------------------------------------  
	public function checkCompatibility($ver1, $ver2)
	//----------------------------------------------------------------------------  
	{
		$modCheckFile = new CheckFile;
		$modCheckFile->version = $ver1;

		return $modCheckFile->getIsCompatibleWith($ver2);
	}
}

?>