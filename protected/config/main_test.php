<?php

return array(
		'tests' => array(
			'unit.*'),

		'components'=>array(
			'fixture'=>array(
				'class'=>'system.test.CDbFixtureManager',
			),
			'db'=>array(
				'database' => 'dw_dm_1_tst',
				'username' => 'root',
				'password' => '123'),
		),
	);
