<?php

define ('SITE_DIR', dirname(__FILE__) . '/../../');

// uncomment the following to define a path alias
Yii::setPathOfAlias('bootstrap', 'protected/components/bootstrap');
Yii::setPathOfAlias('base', dirname(__FILE__) . '/../../../../');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return CMap::mergeArray(array(
	'basePath' => SITE_DIR . 'protected/',
	'name' => 'DM 2',
	'language' => 'ru',
	'defaultController' => 'cp',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.components.*'

	),

	'modules'=>array(
		'backuper' => array('serverName' => 'LCH'),
		'projects', 
	),

	// application components
	'components'=>array(
		'bootstrap'=>array(
			// Если нужно делать кастомизацию, то: debug = true, less-enabled = true, less в прелоад
			'class'=>'application.components.bootstrap.components.Bootstrap', // assuming you extracted bootstrap under extensions
			'debug' => false,
			'cssFile' => 'bootstrap.less.css',
			'coreCss' => false, // Иначе плохо работает Gii 
			'yiiCss' => false,  // Иначе плохо работает Gii 
		),	

		'format' => array('class' => 'DFormatter'),

	
		'db'=>array(
			'class' => 'DDbConnection',
			'emulatePrepare' => false,
			'charset' => 'utf8'),

		'authManager' => array(
			'class' => 'DPhpAuthManager',
			'defaultRoles' => array('guest')),

		'errorHandler' => array('errorAction'=>'cp/error'),
		
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),						
			),
		),

		'urlManager'=>array(
			'urlFormat'=>'path',
			'showScriptName' => false,	// .HTACCESS!!!
			'rules'=>array(
				'' => 'projects/project/admin',						// Ну, и на главную страницу сайта
				'<module:\w+>/<controller:\w+>/<action:\w+>' => '/<module>/<controller>/<action>',
			),
		),		
	),

	// Обращение: Yii::app()->params['paramName']
	'params' => array_merge(unserialize(file_get_contents(SITE_DIR . 'protected/runtime/settings.txt')),
		array(
			)
		),
), require(dirname(__FILE__) . '/' . CONFIG_FILE));