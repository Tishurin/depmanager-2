<?php

return array(
    // Гость может только авторизоваться
    'guest' => array(
      'users' => array('login'),
      'cp' => array('error'),
      'site',
    ),

    // Пользователь может смотреть главную страницу
    'webuser' => array(
      'cp', 'users' => array('profile'),
    ),    

    
    // Админу не доступен модуль assist
    'administrator' => array(
      'cp', 'users'
    ),    
);