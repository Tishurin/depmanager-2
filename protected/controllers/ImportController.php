<?php

set_time_limit(0); 


class ImportController extends DcController
{
	public $defaultAction = 'index'; // Действие по умолчанию
	public $dbSrc;
	public $dbDst;

	const ID_SEMINAR = 10;


	//----------------------------------------------------------------------------
	public function actionIndex()
	//----------------------------------------------------------------------------
	// Отображение главной страницы
	{

		d('Import');

		$this->dbDst = Yii::app()->db;

		d('Connect to source database');
		$this->dbSrc = new CDbConnection('mysql:host=localhost;dbname=dw_dubus_1_dlp', 'root', '123');
		$this->dbSrc->active=true;
		$this->dbSrc->createCommand('SET NAMES UTF8')->execute();

		$this->importPages();
		$this->importBanners();

		/*
		$this->importFiles();
		$this->importPagesFiles();
		$this->importComments();
		$this->importUsers();
		*/

		d('Import done!');
		$this->disableLogs();
		die();
	}

	//----------------------------------------------------------------------------
	public function importUsers()
	//----------------------------------------------------------------------------
	{
		d('Import users');
		$this->dbDst->createCommand('TRUNCATE TABLE tbl_users')->execute();

		// Записи
		$arrRecords = $this->dbSrc->createCommand('SELECT * FROM tbl_users')->queryAll();
		
		foreach ($arrRecords as $sttRecord)
		{
			if($sttRecord['url_role'] == 'user') $sttRecord['url_role'] = 'webuser';
			$modRecord = new User('import');
			$modRecord->attributes = $sttRecord;
			if(!$modRecord->save()) d($modRecord->errors);
		}


		/*

		// Новая
		  `title` varchar(255) NOT NULL,
		  `region` varchar(255) NOT NULL,
		  `skype` varchar(255) NOT NULL,
		  `phone` varchar(255) NOT NULL,
		  `tst_last_auth` int(10) unsigned NOT NULL,
		  `cnt_auth` int(10) unsigned NOT NULL,
		  `birthday_d` int(10) unsigned NOT NULL,
		  `birthday_m` int(10) unsigned NOT NULL,
		  `birthday_y` int(10) unsigned NOT NULL,
		  PRIMARY KEY  (`id`)
		) ENGINE=MyISAM DEFAULT CHARSET=utf8;

		// Старая
		  `title` varchar(255) NOT NULL,
		  `region` varchar(255) NOT NULL,
		  `skype` varchar(255) NOT NULL,
		  `phone` varchar(255) NOT NULL,
		  `tst_last_auth` int(10) unsigned NOT NULL,
		  `cnt_auth` int(10) unsigned NOT NULL,
		  `birthday_d` int(10) unsigned NOT NULL,
		  `birthday_m` int(10) unsigned NOT NULL,
		  `birthday_y` int(10) unsigned NOT NULL,
		  PRIMARY KEY  (`id`)
		) ENGINE=MyISAM AUTO_INCREMENT=103 DEFAULT CHARSET=utf8;

		*/
	}

	//----------------------------------------------------------------------------
	public function importBanners()
	//----------------------------------------------------------------------------
	{
		d('Import banners');
		$this->dbDst->createCommand('TRUNCATE TABLE tbl_banners')->execute();

		// Записи
		$arrRecords = $this->dbSrc->createCommand('SELECT * FROM content_banners')->queryAll();

		$validator = new DFileValidator;	

		foreach ($arrRecords as $sttRecord)
		{
			$modFile = new File('insertDirect');
			$modFile->source = $sttRecord['image'];
			$modFile->name = 'Баннер № ' . $sttRecord['pk_banner'];
	
    		$modFile->flg_image = $validator->checkImage($modFile->sourcePath, &$arrErrors);

			if(!$modFile->save()) d($modFile->errors);
			else $sttRecord['id_image'] = $modFile->id;

			$modRecord = new Banner('import');
			$modRecord->attributes = $sttRecord;
			if(!$modRecord->save()) d($modRecord->errors);
		}


	}

	//----------------------------------------------------------------------------
	public function importPages()
	//----------------------------------------------------------------------------
	{
		d('Import pages');
		$this->dbDst->createCommand('TRUNCATE TABLE tbl_pages')->execute();
		$this->dbDst->createCommand('TRUNCATE TABLE tbl_files')->execute();
		$this->dbDst->createCommand('TRUNCATE TABLE tbl_pages_files')->execute();

		// Папки
		$arrRecords = $this->dbSrc->createCommand('SELECT pk_tree as id, fk_tree as id_parent, name as title, url, 1 as flg_folder FROM content_pages_tree WHERE pk_tree NOT IN (1)')->queryAll();
		foreach ($arrRecords as $sttRecord)
		{
			if ($sttRecord['url'] == 'news') $sttRecord['alias'] = 'news';

			if (in_array($sttRecord['url'], array('illustrations', 'design', 'site')))
				$sttRecord['template'] = 'viewFolderIcons';


			$modPage = new Page('import');
			$modPage->attributes = $sttRecord;
			if(!$modPage->save()) d($modPage->errors);
		}


		// Страницы
		$validator = new DFileValidator;	

		$arrRecords = $this->dbSrc->createCommand('SELECT fk_tree as id_parent, url, title, content, date as tst_create, description, sort as id_sort, image, flg_main FROM content_pages WHERE pk_page NOT IN (3)')->queryAll();
		foreach ($arrRecords as $sttRecord)
		{
			if ($sttRecord['url'] == 'index') $sttRecord['alias'] = 'index';
			if ($sttRecord['id_parent'] == 1) $sttRecord['id_parent'] = 0;

			$arrFiles = array();
			$matches = null;
			$cntMatch = preg_match_all('/<img alt=\"\" src=\"\/var\/files\/(.+?)\">/mui', $sttRecord['content'], &$matches);
			if ($cntMatch > 0) 
			{
				for ($i=0; $i < $cntMatch; $i++) { 
					$sttRecord['content'] = str_replace($matches[0][$i], '<img src="/assets/files/' . $matches[1][$i] . '_real">', $sttRecord['content']);
					$modFile = new File('insertDirect');
					$modFile->name = 'Картинка "' . $sttRecord['title'] . '" № ' . ($i + 1);
					$modFile->source = $matches[1][$i];
					$modFile->flg_image = $validator->checkImage($modFile->sourcePath, &$arrErrors);
					if(!$modFile->save()) d($modFile->errors);
					else $arrFiles[] = $modFile->id;
				}
			}

			if ($sttRecord['image'] != '')
			{
				$modFile = new File('insertDirect');
				$modFile->source = $sttRecord['image'];
				$modFile->name = 'Иконка ' . $sttRecord['title'];
		
        		$modFile->flg_image = $validator->checkImage($modFile->sourcePath, &$arrErrors);

				if(!$modFile->save()) d($modFile->errors);
				else $sttRecord['id_image'] = $modFile->id;
			}

			$modPage = new Page('import');
			$modPage->attributes = $sttRecord;
			if(!$modPage->save()) d($modPage->errors);

			if (count($arrFiles) > 0) {
				foreach ($arrFiles as $id_file) {
					$modPageFile = new PageFile;
					$modPageFile->id_page = $modPage->id;
					$modPageFile->id_file = $id_file;
					$modPageFile->save();
				}
			}
		}

		// Обратная связь
		$modPage = new Page('import');
		$modPage->attributes = array('title' => 'Обратная связь', 'url' => 'feedback', 'alias' => 'feedback', 'content' => 'Это текст страницы про обратную связь');
		if(!$modPage->save()) d($modPage->errors);

		/*

			// Старая
			`pk_page` int(10) unsigned NOT NULL auto_increment,
			`fk_tree` int(10) unsigned NOT NULL,
			`url` varchar(255) NOT NULL,
			`title` varchar(255) NOT NULL,
			`content` text NOT NULL,
			`sort` int(10) unsigned NOT NULL,
			`hidden` tinyint(3) unsigned NOT NULL,
			`content_typo` text NOT NULL,
			`url_full` varchar(45) NOT NULL,
			`description` text NOT NULL,
			`date` int(10) unsigned NOT NULL,
			`template` varchar(45) NOT NULL,
			`flg_main` int(10) unsigned NOT NULL,
			`image` varchar(255) NOT NULL,

			// Новая
		  `id` int(10) unsigned NOT NULL auto_increment,
		  `id_parent` int(10) unsigned NOT NULL,
		  `id_sort` int(10) unsigned NOT NULL,
		  `tst_create` int(10) unsigned NOT NULL,
		  `tst_update` int(10) unsigned NOT NULL,
		  `flg_public` int(10) unsigned NOT NULL,
		  `flg_folder` int(10) unsigned NOT NULL,
		  `url` varchar(255) NOT NULL,
		  `title` varchar(255) NOT NULL,
		  `alias` varchar(255) NOT NULL,
		  `template` varchar(255) NOT NULL,
		  `keywords` text NOT NULL,
		  `description` text NOT NULL,
		  `content` text NOT NULL,
		  `areas` text NOT NULL,
		  `flg_block_header` int(10) unsigned NOT NULL,
		  `flg_block_text` int(10) unsigned NOT NULL,



		*/
	}


	//----------------------------------------------------------------------------
	public function importFiles()
	//----------------------------------------------------------------------------
	{
		d('Import files');
		$this->dbDst->createCommand('TRUNCATE TABLE tbl_files')->execute();

		$path = Yii::getPathOfAlias('application.data') . '/files';

		// Записи
		$arrRecords = $this->dbSrc->createCommand('SELECT id, name, file as source, id_folder as id_parent, flg_folder, tst_create as tst_upload, description FROM tbl_files')->queryAll();
		foreach ($arrRecords as $sttRecord)
		{

			if (strlen($sttRecord['source']) == 13)
			{
				$tmp = $sttRecord['source'];
				DFileHelper::mkdir($path . '/' . $sttRecord['source'][4], 0777);
				$sttRecord['source'] = $sttRecord['source'][4] . '/' . $sttRecord['source'];

				if (is_file($path . '/' . $tmp) && !is_file($path . '/' . $sttRecord['source']))
					copy($path . '/' . $tmp, $path . '/' . $sttRecord['source']);
			}

			$modRecord = new File('import');
			$modRecord->attributes = $sttRecord;
			if(!$modRecord->save()) d($modRecord->errors);
		}


		/*
		Старая:
		  `size` int(10) unsigned NOT NULL,
		  `cnt_download` int(10) unsigned NOT NULL,
		  `description` text NOT NULL,

		Новая:
		  `flg_image` int(10) unsigned NOT NULL,
		*/
	}


	//----------------------------------------------------------------------------
	public function importPagesFiles()
	//----------------------------------------------------------------------------
	{
		d('Import pages-files');
		$this->dbDst->createCommand('TRUNCATE TABLE tbl_pages_files')->execute();

		// Файлы
		$arrRecords = $this->dbSrc->createCommand('SELECT id_article as id_page, id_file FROM tbl_articles_files WHERE id_article <= 223 ')->queryAll();
		foreach ($arrRecords as $sttRecord)
		{
			$modRecord = new PageFile('import');
			$modRecord->attributes = $sttRecord;
			if(!$modRecord->save()) d($modRecord->errors);
		}

		// Картинки
		$arrRecords = $this->dbSrc->createCommand('SELECT id_article as id_page, id_file FROM tbl_articles_images WHERE id_article <= 223')->queryAll();
		foreach ($arrRecords as $sttRecord)
		{
			$modRecord = new PageFile('import');
			$modRecord->attributes = $sttRecord;
			if(!$modRecord->save()) d($modRecord->errors);
		}
	}
}