<?php

// alias: components.CpController
// version: 1.0.0
// dependencies:
// components.DcController 1.0
// enddep

class CpController extends DcController
{
  public $defaultAction = 'index'; // Действие по умолчанию

  /*
  public function init()
  {
    Yii::app()->onError = function ($event) {echo 'Ошибка в контрольной панели'; die();}
  }
  */

  //----------------------------------------------------------------------------
	public function actionIndex()
  //----------------------------------------------------------------------------
	// Отображение главной страницы
  {
    $this->pageTitle = 'Главная страница';
    $this->render('index');
	}




}