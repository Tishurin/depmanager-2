<?php

// alias: components.DActiveRecordTreeBehavior
// version: 1.0.1

class DActiveRecordTreeBehavior extends CActiveRecordBehavior
{
	public $idParent;
	public $strUrl = 'url';	// Поле для обозначения Урла

	private $_img;
	private $_imgFull;

	public $_parents;
	private $_children;
	private $_allChildrenPk;
	private $_allChildren;

	//----------------------------------------------------------------------------
	public function getImg()
	//----------------------------------------------------------------------------
	// Все записи
	{
		if ($this->_img === null)
		{
			$cmd = Yii::app()->db->createCommand()
						 ->select()
						 ->from($this->owner->tableName());

			if ($this->owner->order !== null)
				$cmd->order($this->owner->order);


			$dr = $cmd->query();
			
			$this->_img = array();
			while ($t = $dr->read()) $this->_img[$t[$this->owner->getMetaData()->tableSchema->primaryKey]] = $t; 
		}
		return $this->_img;
	}

	//----------------------------------------------------------------------------
	public function getAllChildrenPk()
	//----------------------------------------------------------------------------  
	// Первичные ключи всех потомков
	{
		if ($this->_allChildrenPk === null)
			$this->_allChildrenPk = $this->_allChildrenPkR($this->owner->id);
		
		return $this->_allChildrenPk;
	}

	//----------------------------------------------------------------------------
	private function _allChildrenPkR($id)
	//----------------------------------------------------------------------------  
	{
		$arrRet = array();
		foreach ($this->img as $k => $v)
			if ($v[$this->idParent] == $id)
			$arrRet = array_merge($arrRet, array($k), $this->_allChildrenPkR($k));
		return $arrRet;
	}


	//----------------------------------------------------------------------------
	public function findAllChildren()
	//----------------------------------------------------------------------------  
	// Первичные ключи всех потомков
	{
		if ($this->_allChildrenPk === null);
			$this->_allChildrenPk = $this->owner->populateRecords($this->findAllChildrenR($this->owner->id));
		
		return $this->_allChildrenPk;
	}

	//----------------------------------------------------------------------------
	private function findAllChildrenR($id)
	//----------------------------------------------------------------------------  
	{
		$arrRet = array();
		foreach ($this->img as $k => $v)
		{
			if ($v[$this->idParent] == $id)
				$arrRet = array_merge($arrRet, array($v), $this->findAllChildrenR($k));
		}

		return $arrRet;
	}

	//----------------------------------------------------------------------------
	public function beforeDelete($event)
	//----------------------------------------------------------------------------  
	// Удалить всех потомков, в событии сохранить ключи удаленных узлов
	{
		$arrNodeKeys = $this->owner->getAllChildrenPk();
		$this->owner->deleteByPk($arrNodeKeys);
		$event->params['arrDeleteNodes'] = array_merge(array($this->owner->oldPrimaryKey), $arrNodeKeys);
	}

	//----------------------------------------------------------------------------
	public function findArrTree($node = 0, $level = 0)
	//----------------------------------------------------------------------------
	// Функция для виджета "дерево"
	{
		if($level == 0) $level=-1;
		return $this->findArrTreeR($node, $level);
	}

	//----------------------------------------------------------------------------
	private function findArrTreeR($node, $level)
	//----------------------------------------------------------------------------  
	{
		$r = array();
		if($level == 0)return $r;
		$level--;
		foreach($this->img as $k => $v)
		{
			if($v[$this->idParent] == $node)
			{
				$r[$k]['id'] = $v['id'];
				$r[$k]['value'] = $v;
				$r[$k]['children'] = $this->findArrTreeR($k, $level);
			}
		}
		return $r;
	}

	//----------------------------------------------------------------------------
	public function getChildren($flgWithParents = true)
	//----------------------------------------------------------------------------
	// Прямые потомки
	{
		$arrSearch = array();
		if ($this->owner->order !== null)
			$arrSearch['order'] = $this->owner->order;


		$arrChildren = $this->owner->findAllByAttributes(array($this->idParent => $this->owner->id), $arrSearch);

		if ($flgWithParents)
		{
			$arrParents = $this->parents;
			$arrParents[] = $this->owner;

			foreach ($arrChildren as $k => $modChildren)
				$arrChildren[$k]->_parents = $arrParents;
		}


		return $arrChildren;
	}

	//----------------------------------------------------------------------------
	public function getParents()
	//----------------------------------------------------------------------------
	{
		if ($this->_parents === null)
		{
			$strParent = $this->idParent;
			$id = $this->owner->$strParent;

			$arrRet = array();
			while($id != 0)
			{
				$arrRet[] = $this->img[$id];
				$id = $this->img[$id]['id_parent'];
			}
			$this->_parents = $this->owner->populateRecords(array_reverse($arrRet));
		}


		return $this->_parents;
	}
}

