<?php

// alias: components.DActiveRecordSelectBehavior
// version: 1.0.0

class DActiveRecordSelectBehavior extends CActiveRecordBehavior
{

	//----------------------------------------------------------------------------
	public function selectByPk($id)
	//----------------------------------------------------------------------------
	{
		$_SESSION[$this->sessionName][$id] = 1;
	}

	//----------------------------------------------------------------------------
	public function unselectByPk($id)
	//----------------------------------------------------------------------------
	{
		unset($_SESSION[$this->sessionName][$id]);
	}

	//----------------------------------------------------------------------------
	public function getSelectedPk()
	//----------------------------------------------------------------------------
	{
		if (!isset($_SESSION[$this->sessionName]))
			$_SESSION[$this->sessionName] = array();
		return array_keys($_SESSION[$this->sessionName]);
	}

	//----------------------------------------------------------------------------
	public function unselectAll()
	//----------------------------------------------------------------------------
	{
		$_SESSION[$this->sessionName] = array();
	}

	//----------------------------------------------------------------------------
	public function getHasSelection()
	//----------------------------------------------------------------------------
	{
		return (count($this->selectedPk) > 0);
	}	

	//----------------------------------------------------------------------------
	public function getIsSelected()
	//----------------------------------------------------------------------------
	{
		return isset($_SESSION[$this->sessionName][$this->owner->id]);
	}

	//----------------------------------------------------------------------------
	public function getSessionName()
	//----------------------------------------------------------------------------
	{
		return get_class($this->owner) . 'Select';
	}
}

