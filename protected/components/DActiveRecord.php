<?php

// alias: components.DActiveRecord
// version: 1.1.0

class DActiveRecord extends CActiveRecord
{
	public static function model ($className=__CLASS__) {return parent::model($className);}


	private $_sort;
	private $_pagination;


	//----------------------------------------------------------------------------  
	public function getSortConfig() 
	//----------------------------------------------------------------------------
	{
		return $this->_sort;
	}
	
	//----------------------------------------------------------------------------  
	public function setSort($sttSort)
	//----------------------------------------------------------------------------  
	{
		$this->_sort = $sttSort;
	}

	//----------------------------------------------------------------------------
	public function getPaginationConfig()
	//----------------------------------------------------------------------------  
	{
		return $this->_pagination;
	}

	//----------------------------------------------------------------------------  
	public function setPagination($sttConfig)
	//----------------------------------------------------------------------------  
	{
		$this->_pagination = $sttConfig;
	}


	//----------------------------------------------------------------------------
	public function search()
	//----------------------------------------------------------------------------	
	// Универсальная функция поиска, работающая от свойств модели
	{
		$obj_provider = new CActiveDataProvider($this, array(
			'sort' => $this->sortConfig,
			'pagination' => $this->paginationConfig,
		));

		return $obj_provider;
	}




	//----------------------------------------------------------------------------
	public function timestamp($attribute, $params)
	//----------------------------------------------------------------------------  
	// Валидация даты
	{
		$obj_validator = new DTimestampValidator();
		return $obj_validator->validateAttribute($this, $attribute); 
	}  



}

