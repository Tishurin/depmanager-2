<?php

// alias: components.DFileHelper
// version: 1.0.0

//*****************************************************************************
class DFileHelper extends CFileHelper
//*****************************************************************************
{

	//----------------------------------------------------------------------------  
	public static function ls($strPath)
	//----------------------------------------------------------------------------  
	// Выдает сассив файлов директории без . и ..
	{
		$arrRet = array();
		if ($handle = opendir($strPath))
		{
		    while (false !== ($strFile = readdir($handle)))
		    	if ($strFile != '.' && $strFile != '..')
		    		$arrRet[] = $strFile; 
		    closedir($handle);
		}
		return $arrRet;
	}

	//----------------------------------------------------------------------------  
	public static function lsA($strAlias)
	//----------------------------------------------------------------------------  
	{
		return self::ls(self::path($strAlias));
	}


	//----------------------------------------------------------------------------  
	public static function mkdir($strPath, $intMode = 0777)
	//----------------------------------------------------------------------------  
	{
		if (is_dir($strPath)) return false;
		else return mkdir($strPath, $intMode);
	}

	//----------------------------------------------------------------------------  
	public static function mkdirA($strAlias, $intMode = 0777)
	//----------------------------------------------------------------------------  
	{
		return self::mkdir(self::path($strAlias), $intMode);
	}


	//----------------------------------------------------------------------------  
	public static function put($strPath, $strContent)
	//----------------------------------------------------------------------------  
	{
		return file_put_contents($strPath, $strContent);
	}

	//----------------------------------------------------------------------------  
	public static function putA($strAlias, $strFile, $strContent)
	//----------------------------------------------------------------------------  
	{
		return file_put_contents(self::path($strAlias, $strFile), $strContent);
	}


	//----------------------------------------------------------------------------  
	public static function get($strPath)
	//----------------------------------------------------------------------------  
	{
		return file_get_contents($strPath);
	}

	//----------------------------------------------------------------------------  
	public static function getA($strAlias, $strFile)
	//----------------------------------------------------------------------------  
	{
		return file_get_contents(self::path($strAlias, $strFile));
	}

	//----------------------------------------------------------------------------  
	public static function deleteA($strAlias, $strFile = '')
	//----------------------------------------------------------------------------  
	{
		return self::delete(self::path($strAlias, $strFile));
	}

	//----------------------------------------------------------------------------  
	public static function delete($strPath)
	//----------------------------------------------------------------------------  
	{
		if (is_file($strPath)) return unlink($strPath);

		if (is_dir($strPath))
		{
			foreach (self::ls($strPath) as $strFile)
				self::delete($strPath . '/' . $strFile);

			rmdir($strPath);
		}
	}

	public static function copy($src, $dst)
	{
		return copy($src, $dst);
	}

	public static function copyA($srcAlias, $srcFile, $dstAlias, $dstFile = '')
	{
		if ($dstFile == '') $dstFile = $srcFile;
		return copy(self::path($srcAlias, $srcFile), self::path($dstAlias, $dstFile));
	}

	//----------------------------------------------------------------------------  
	public static function path($strAlias, $strFile = '')
	//---------------------------------------------------------------------------- 
	// По алиасу и имени файла выдает полный путь 
	{
		return Yii::getPathOfAlias($strAlias) . ($strFile == '' ? '' : '/' . $strFile);
	}
}


?>