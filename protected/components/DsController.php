<?php

// alias: components.DsController
// version: 1.0.d.0
// dependencies:
// components.DController 1.0
// enddep

class DsController extends DController
{
	public $layout = '//layouts/site';
  	public $errorAction = 'site/site/error';
  	public $showBreadcrumbs = true;
}
