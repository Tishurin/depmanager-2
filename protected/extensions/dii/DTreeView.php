<?php
class DTreeView extends CTreeView
{
	public $nodeTemplate;


	//----------------------------------------------------------------------------
	public function init()
	//----------------------------------------------------------------------------
	{
		if(isset($this->htmlOptions['id']))
			$id=$this->htmlOptions['id'];
		else
			$id=$this->htmlOptions['id']=$this->getId();
		if($this->url!==null)
			$this->url=CHtml::normalizeUrl($this->url);
		$cs=Yii::app()->getClientScript();

		$cs->registerCoreScript('treeview');	// Зарегистрировали JS для дерева

		$diiAssest = Yii::app()->assetManager->publish(Yii::getPathOfAlias('ext.dii.assets')); // Опубликовали наши картинки


		$options=$this->getClientOptions();
		$options=$options===array()?'{}' : CJavaScript::encode($options);

		$cs->registerScript('Yii.CTreeView#'.$id,"jQuery(\"#{$id}\").treeview($options);");

		if($this->cssFile===null)
			$cs->registerCssFile($diiAssest . '/dii.treeview.css');	// Подключили наш файл CSS
		else if($this->cssFile!==false)
			$cs->registerCssFile($this->cssFile);
		

			

		echo CHtml::tag('ul',$this->htmlOptions,false,false)."\n";
		$i = 0;
		echo self::saveDataAsHtml($this->data, $i, $this->nodeTemplate);
	}
	
	//----------------------------------------------------------------------------
	public static function saveDataAsHtml($data, $i = 0, $nodeTemplate = null)
	//----------------------------------------------------------------------------
	{
    	$i_store = $i;
    	$html='';
		if(is_array($data))
		{
			
			foreach($data as $node)
			{
				if(!isset($node['text']) && !isset($node['value']))
					continue;

				if(isset($node['expanded']))
					$css=$node['expanded'] ? 'open' : 'closed';
				else
					$css='';


				if(isset($node['hasChildren']) && $node['hasChildren'])
				{
					if($css!=='')
						$css.=' ';
					$css.='hasChildren';
				}

				$options = isset($node['htmlOptions']) ? $node['htmlOptions'] : array();
				
				if($css!=='')
				{
					if(isset($options['class']))
						$options['class'].=' '.$css;
					else
						$options['class']=$css;
				}



				if(isset($node['id']))
					$options['id']=$node['id'];

				if ($i == 0) $i++;
				else $i = 0;
				$class = $i == 0 ? 'odd' : 'even';



				$strContent = $nodeTemplate === null ? $node['text'] : Yii::app()->controller->renderPartial($nodeTemplate, array('data' => $node['value']), true);
        		

        		$html .= CHtml::tag('li', $options, CHtml::tag('div', array('class' => 'treecontent ' . $class), $strContent), false);
				


				if(!empty($node['children']))
				{
					$html.="\n<ul>\n";
					$html.=self::saveDataAsHtml($node['children'], $i, $nodeTemplate);
					$html.="</ul>\n";
				}
				$html.=CHtml::closeTag('li')."\n";
			}
			if ($i != $i_store)
			{
				
				if ($i == 1) $class = 'odd';
				else $class = 'even';
				//$html .= CHtml::tag('div', array('style' => 'margin-left: 25px; padding: 0px; line-height: 10px;', 'class' => 'treecontent ' . $class), '&nbsp;');

			}
				
		}
		return $html;
	}

}
