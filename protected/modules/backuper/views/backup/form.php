

<?php $form = $this->beginWidget('DActiveForm', array(
    'id'=>'user-form',
    'htmlOptions' => array('class' => 'form-horizontal',
                           'enctype' => 'multipart/form-data'),
    'enableAjaxValidation'=>false,
    'enableClientValidation'=>false,
    'focus'=>array($model,'name'),
)); ?>

<?php if(count($model->errors) > 0): ?>
<div class="alert alert-error">
  <?php echo $form->errorSummary($model); ?>
</div>
<?php endif; ?>

<fieldset>

  <?php foreach ($model->attributeInputs() as $str_attribute => $stt_input): ?>

    <?php
     if (!is_array($stt_input)) $stt_input = array('type' => $stt_input);
     $str_type = $stt_input['type'];
     
     

     if (in_array($str_type, array('checkBox', 'checkBoxList')))
     {
      $defHtmlOptions = array('style' =>'width: 100px;');
     }
     else $defHtmlOptions = array('class' => 'span10');
     
     $param_3 = (isset($stt_input['data']) ? $stt_input['data'] : (isset($stt_input['htmlOptions']) ? $stt_input['htmlOptions'] : $defHtmlOptions));
     $param_4 = (isset($stt_input['data']) && isset($stt_input['htmlOptions']) ? $stt_input['htmlOptions'] : $defHtmlOptions);
    ?>

    <div class="control-group <?php if (isset($model->errors[$str_attribute])) echo 'error'; ?>">
        <?php echo $form->labelEx($model, $str_attribute, array('class' => 'control-label')); ?>
        <div class='controls'>
          <?php echo $form->$str_type($model, $str_attribute, $param_3, $param_4); ?>
          <?php echo $form->error($model, $str_attribute, array('class' => 'help-block')); ?>
        </div>
    </div>


  <?php endforeach;?>


</fieldset>

<div class="form-actions">
    <?php $this->widget('bootstrap.widgets.BootButton', array('buttonType'=>'submit', 'type'=>'primary', 'icon'=>'ok white', 'label'=>'ОК')); ?>
    <?php //$this->widget('bootstrap.widgets.BootButton', array('buttonType'=>'reset', 'icon'=>'remove', 'label'=>'Reset')); ?>
</div>



<?php $this->endWidget(); ?>

