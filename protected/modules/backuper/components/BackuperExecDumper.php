<?php

class BackuperExecDumper
{
  //----------------------------------------------------------------------------  
  public function dump($arrTables, $strFile)
  //----------------------------------------------------------------------------  
  {
    $strTables = implode(' ', $arrTables);
    exec('mysqldump -u' . Yii::app()->db->username . ' --password=' . Yii::app()->db->password . ' ' . Yii::app()->db->database . ' ' . $strTables . ' > ' . $strFile);
    return 0;
  }

  //----------------------------------------------------------------------------
  public function retreive($strFile)
  //----------------------------------------------------------------------------  
  {
    exec('mysql -u' . Yii::app()->db->username . ' --password=' . Yii::app()->db->password . ' ' . Yii::app()->db->database . ' < ' . $strFile);
  }

}

?>