<?php

Yii::setPathOfAlias('var', Yii::app()->basePath . '/../var/');

class BackuperModule extends CWebModule
{
  public $serverName;                           // Код сервера (LCH или SRV) 
  public $dumperClass = 'BackuperExecDumper';   // Используемый бекапер (на PHP или на mysqldump)

	public function init()
	{
		// import the module-level models and components
		$this->setImport(array(
			'backuper.models.*',
			'backuper.components.*',
		));
	}


}
