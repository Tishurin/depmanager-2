<?php

// alias: modules.backuper.controllers.backup
// version: 1.0.0

class BackupController extends DcController
{
  // Действие по умолчанию
  public $defaultAction = 'admin';                
  public $defaultActionName = 'Резервные копии';  

  //----------------------------------------------------------------------------
  public function loadModel($urlBackup)
  //----------------------------------------------------------------------------
  {
    $objBackup = new BackuperBackup; 
    $modBackup = $objBackup->findByPk($urlBackup);
    if ($modBackup === null)
      throw new CHttpException(404, 'Бекап не найден');
      
    return $modBackup;
  }
  
  //----------------------------------------------------------------------------
  public function actionCreate()
  //----------------------------------------------------------------------------  
  {
    $this->pageTitle = 'Новый бекап';
    
    $modBackup = new BackuperBackup;
    $modBackup->server_name = $this->module->serverName;
    $modBackup->getEnumTables();

    // Выполнить сохранение
		if(isset($_POST['BackuperBackup']))
		{
      $modBackup->attributes = $_POST['BackuperBackup'];
      if($modBackup->save()) $this->redirect(array('admin'));
		}

    // Передать контроллеру    
		$this->render('form',array(
			'model'=>$modBackup,
		));

  }

  //----------------------------------------------------------------------------
  public function actionAdmin()
  //----------------------------------------------------------------------------
  {
    $this->pageTitle = 'Резервные копии';
    
    $modBackup = new BackuperBackup;
    
        
    $dpBackups = new CArrayDataProvider($modBackup->findAll(), array(
        'id' => 'BackuperBackups',
        'sort' => false,
        'pagination' => false
    ));
    
    
		$this->render('admin', array('dpBackups' => $dpBackups));
  }

  //----------------------------------------------------------------------------
  public function actionView($urlBackup)
  //----------------------------------------------------------------------------  
  {
    $modBackup = $this->loadModel($urlBackup);
    
    $this->breadcrumbs = array('Резервные копии' => array('admin'));
    $this->pageTitle = $modBackup->name;
    
    $tmp = $this->menu;
    $this->_menu[] = array('label'=>'Восстановить бекап', 
            'icon' => 'refresh', 
            'url' => '#',
            'linkOptions'=>array('submit'=>array('retreive', 'urlBackup' => $urlBackup), 'confirm'=>'Точно?'),
            'visible' => (Yii::app()->user->checkAccess('assist/backuper/backup/create')),
            );
    
    
    $this->render('view', array('model' => $modBackup, 'attributes' => null));
  }

  //----------------------------------------------------------------------------
  public function actionRetreive($urlBackup)
  //----------------------------------------------------------------------------  
  {
    $objBackup = new BackuperBackup;
    $modBackup = $objBackup->retreiveByPk($urlBackup);

    
    Yii::app()->user->setFlash('info', 'Бекап ' . $urlBackup . ' восстановлен');
    $this->redirect(array('admin'));
  }
  
  //----------------------------------------------------------------------------  
  public function actionDelete($urlBackup)
  //----------------------------------------------------------------------------  
  {
    if(Yii::app()->request->isPostRequest)
		{
      $objBackup = new BackuperBackup;
      $modBackup = $objBackup->deleteByPk($urlBackup);

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
			{
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
			}
		}
		else throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
  }

  //----------------------------------------------------------------------------  
  public function initMenu()
  //----------------------------------------------------------------------------  
  {
    return array(
             array('label'=>'Сделать бекап', 
                   'icon' => 'plus', 
                   'url' => array('create'), 
                   'visible' => (Yii::app()->user->checkAccess('assist/backuper/backup/create')))
          );  
  }



}