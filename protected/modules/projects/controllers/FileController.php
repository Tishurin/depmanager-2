<?php

class FileController extends DcController
{
	public $defaultAction = 'admin';
	public $defaultActionName = 'Файлы';

	//----------------------------------------------------------------------------
	public function actionAdmin()
	//----------------------------------------------------------------------------
	{
		$modFile = new File('search');
		$modFile->unsetAttributes();
		
		if(isset($_GET['File'])) $modFile->attributes = $_GET['File'];


		$this->pageTitle = 'Файлы';
		$this->menu = array(                          
										array('label' => 'Добавить', 
													'icon' => 'plus',
													'url' => array('/projects/file/create'), 
													'visible' => Yii::app()->user->checkAccess('projects/file/create')),
		);


		$this->render('admin', array('modFile' => $modFile));
	}

	//----------------------------------------------------------------------------
	public function actionCopy($id, $id_to)
	//----------------------------------------------------------------------------
	{
		$modFrom = $this->loadModel($id);
		$modTo = $this->loadModel($id_to);

		copy($modFrom->fullPath, $modTo->fullPath);

		$this->redirectBack();		
	}

	//----------------------------------------------------------------------------
	public function actionView($id, $flg_no_project = 0)
	//----------------------------------------------------------------------------
	{
		$modFile = $this->loadModel($id);

		if (!$flg_no_project) {
			$this->breadcrumbs = array(
				'Проекты' => array('/projects/project/admin'),
				$modFile->project->name => array('/projects/project/view', 'id' => $modFile->project->id),
				);
		}


		$this->pageTitle = array_pop(explode('/', $modFile->path)) . ' ' . $modFile->version;

		$this->menu = array();
		
		$this->render('view', array('modFile' => $modFile, 'flg_no_project' => $flg_no_project));
	}

	//----------------------------------------------------------------------------
	public function actionCreate()
	//----------------------------------------------------------------------------
	{
		$modFile = new File;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['File']))
		{
			$modFile->attributes = $_POST['File'];
			if($modFile->save())
				$this->redirectPageStatePrevious();
		}
		else $this->initPageStatePrevious();

		$this->pageTitle = 'Добавить';

		$this->render('create', array('modFile' => $modFile));
	}

	//----------------------------------------------------------------------------
	public function actionUpdate($id)
	//----------------------------------------------------------------------------
	{
		$modFile = $this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['File']))
		{
			$modFile->attributes = $_POST['File'];
			if($modFile->save())
				$this->redirectPageStatePrevious();
		}
		else $this->initPageStatePrevious(array('view', 'id' => $modFile->id)); // Переопределите если нужно


		$this->pageTitle = 'Редактировать';

		$this->render('update', array('modFile' => $modFile));
	}

	//----------------------------------------------------------------------------
	public function actionDelete($id)
	//----------------------------------------------------------------------------  
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Ошибка запроса.');
	}

	//----------------------------------------------------------------------------
	public function loadModel($id)
	//----------------------------------------------------------------------------
	{
		$modFile = File::model()->findByPk($id);
		if($modFile === null)
			throw new CHttpException(404,'Такой страницы нет!');
		return $modFile;
	}
}
