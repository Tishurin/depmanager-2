<?php

class ProjectController extends DcController
{
	public $defaultAction = 'admin';
	public $defaultActionName = 'Проекты';

	//----------------------------------------------------------------------------
	public function actionAdmin()
	//----------------------------------------------------------------------------
	{
		$modProject = new Project('search');
		$modProject->unsetAttributes();
		
		if(isset($_GET['Project'])) $modProject->attributes = $_GET['Project'];


		$this->pageTitle = 'Проекты';
		$this->menu = array(                          
										array('label' => 'Добавить', 
													'icon' => 'plus',
													'url' => array('/projects/project/create'), 
													'visible' => Yii::app()->user->checkAccess('projects/project/create')),
		);


		$this->render('admin', array('modProject' => $modProject));
	}

	//----------------------------------------------------------------------------
	public function actionView($id)
	//----------------------------------------------------------------------------
	{
		$modProject = $this->loadModel($id);
		
		$this->pageTitle = $modProject->name;
		$this->menu = array(

								array('label'=>'Редактировать', 
											'icon' => 'pencil',
											'url' => array('/projects/project/update', 'id' => $modProject->id), 
											'visible' => Yii::app()->user->checkAccess('projects/project/update')),
								
								array('label'=>'Удалить', 
											'icon' => 'trash',
											'url' => '#', 
											'linkOptions'=>array('submit'=>array('/projects/project/delete', 'id' => $modProject->id), 'confirm'=>'Точно?'), 
											'visible' => Yii::app()->user->checkAccess('projects/project/delete')),
		);


		$modFile = new File('search');
		$modFile->unsetAttributes();		
		$modFile->id_project = $modProject->id;
		if(isset($_GET['File'])) $modFile->attributes = $_GET['File'];

		
		$this->render('view', array('modProject' => $modProject, 'modFile' => $modFile));
	}

	//----------------------------------------------------------------------------
	public function actionRefreshFileIndex()
	//----------------------------------------------------------------------------
	{
		$countBefore = File::model()->count();

		$arrProjects = Project::model()->active()->findAll();

		foreach ($arrProjects as $modProject)
			$modProject->refreshFileIndex();


		$countAfter = File::model()->count();

		$diff = $countAfter - $countBefore;

		if ($diff == 0) $str = 'Новых файлов вроде нет';
		elseif ($diff > 0) $str = 'Добавлено ' . $diff . ' файлов';
		elseif ($diff < 0) $str = 'Исключено ' . (-$diff) . ' файлов';

		Yii::app()->user->setFlash('info', 'Индекс активных проектов обновлен. ' . $str);


		$this->redirectBack();
	}

	//----------------------------------------------------------------------------
	public function actionCreate()
	//----------------------------------------------------------------------------
	{
		$modProject = new Project;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Project']))
		{
			$modProject->attributes = $_POST['Project'];
			if($modProject->save())
				$this->redirect(array('/projects/project/view', 'id' => $modProject->id));
		}
		else $this->initPageStatePrevious();

		$this->pageTitle = 'Добавить';

		$this->render('create', array('modProject' => $modProject));
	}

	//----------------------------------------------------------------------------
	public function actionUpdate($id)
	//----------------------------------------------------------------------------
	{
		$modProject = $this->loadModel($id);

		$this->breadcrumbs[$modProject->name] = array('/projects/project/view', 'id' => $modProject->id);

		if(isset($_POST['Project']))
		{
			$modProject->attributes = $_POST['Project'];
			if($modProject->save())
				$this->redirectPageStatePrevious();
		}
		else $this->initPageStatePrevious(array('view', 'id' => $modProject->id)); // Переопределите если нужно


		$this->pageTitle = 'Редактировать';

		$this->render('update', array('modProject' => $modProject));
	}

	//----------------------------------------------------------------------------
	public function actionDelete($id)
	//----------------------------------------------------------------------------  
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Ошибка запроса.');
	}

	//----------------------------------------------------------------------------
	public function loadModel($id)
	//----------------------------------------------------------------------------
	{
		$modProject = Project::model()->findByPk($id);
		if($modProject === null)
			throw new CHttpException(404,'Такой страницы нет!');
		return $modProject;
	}
}
