
<?php $form=$this->beginWidget('CActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
	'htmlOptions' => array('style' => 'margin-bottom: 0;', 'class' => 'form-horizontal'),
)); ?>

	<div class='well well-small' style='padding-right: 130px; margin-bottom: 0;'>
		<?php $this->widget('bootstrap.widgets.BootButton', array('buttonType'=>'submit', 'type' => 'primary', 'icon'=>'icon-search icon-white', 'label'=>'Искать', 'htmlOptions' => array('style' => 'float: right; text-align: left; margin-right: -120px; width: 100px;'))); ?> 
		<div class='row-fluid' style='margin-bottom: 0;'>
			<div class='span9'>
				<?php echo $form->textField($modProject, 'request', array('style' => 'margin-bottom: 0; width: 100%;', 'placeholder' => 'Введите фразу для поиска')); ?>
			</div>
			

			<div class='span3'>
				<div class="control-group" style='margin-bottom: 0;'>
					<label class="control-label" for="Project_request_closed">Показывать закрытые</label>
					<div class='controls'>
						<?php echo $form->checkBox($modProject, 'request_closed', array('class' => '')); ?>
					</div>
				</div>
			</div>
		</div>

	</div>
<?php $this->endWidget(); ?>

<style type="text/css">
	.closed_project .name {text-decoration: line-through;}
</style>

<?php $this->widget('bootstrap.widgets.BootGridView', array(
	'id' => 'project-grid',
	'dataProvider' => $modProject->search(),
	'type'=>'striped bordered',
	'template' => '{items}{pager}',
	'rowCssClassExpression' => '$data->isClosed ? "closed_project" : ""',
	'ajaxUpdate' => false,
	'columns'=>array(
		array('name' => 'name', 'htmlOptions' => array('class' => 'name')),
		array(
			'class'=>'bootstrap.widgets.BootButtonColumn',
			'template' => '{view}',
			'htmlOptions' => array('style' => 'text-align: center;'),
		),
	),
)); ?>
