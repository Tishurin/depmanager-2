
<?php $form = $this->beginWidget('DActiveForm', array(
	'id'=>'project-form',
	'stateful' => true,
	'htmlOptions' => array('class' => 'form-horizontal',
												 'enctype' => 'multipart/form-data'),
	'enableAjaxValidation' => false,
	'enableClientValidation' => false,
)); ?>

	<fieldset>

		<div class="control-group">
			<div class='controls'>          
				<?php if(count($modProject->errors) > 0): ?>
					<div class="span10 alert alert-error">
					 <?php echo $form->errorSummary($modProject); ?>
					</div>
				<?php endif ?>
			</div>
		</div>

		<div class='row-fluid'>
			<div class='span6'>
				<!-- NAME -->
				<div class="control-group <?php if (isset($modProject->errors['name'])) echo 'error'; ?> ">
					<?php echo $form->labelEx($modProject, 'name', array('class' => 'control-label')); ?>
					<div class='controls'>
						<?php echo $form->textField($modProject, 'name', array('class' => 'span12')); ?>
						<?php echo $form->error($modProject, 'name', array('class' => 'help-block')); ?>
					</div>
				</div>

			</div>
			<div class='span6'>
				<!-- flg_closed -->
				<div class="control-group <?php if (isset($modProject->errors['flg_closed'])) echo 'error'; ?> ">
					<?php echo $form->labelEx($modProject, 'flg_closed', array('class' => 'control-label')); ?>
					<div class='controls'>
						<?php echo $form->checkBox($modProject, 'flg_closed', array()); ?>
						<?php echo $form->error($modProject, 'flg_closed', array('class' => 'help-block')); ?>
					</div>
				</div>
			</div>			
		</div>
	 


	 
		<!-- PATH -->
		<div class="control-group <?php if (isset($modProject->errors['path'])) echo 'error'; ?> ">
			<?php echo $form->labelEx($modProject, 'path', array('class' => 'control-label')); ?>
			<div class='controls'>
				<?php echo $form->textField($modProject, 'path', array('class' => 'span12')); ?>
				<?php echo $form->error($modProject, 'path', array('class' => 'help-block')); ?>
			</div>
		</div>
	 
		<!-- ignore -->
		<div class="control-group <?php if (isset($modProject->errors['ignore'])) echo 'error'; ?> ">
			<?php echo $form->labelEx($modProject, 'ignore', array('class' => 'control-label')); ?>
			<div class='controls'>
				<?php echo $form->textArea($modProject, 'ignore', array('class' => 'span12', 'style' => 'height: 150px;')); ?>
				<?php echo $form->error($modProject, 'ignore', array('class' => 'help-block')); ?>
				<span class="help-block">Регулярные выражения для полного пути файла</span>
			</div>
		</div>
		
	</fieldset>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.BootButton', array('buttonType'=>'link', 'url' => $this->pageStatePrevious, 'type'=>'danger', 'icon'=>'remove white', 'label'=>'Отмена')); ?>
		<?php $this->widget('bootstrap.widgets.BootButton', array('buttonType'=>'submit', 'type'=>'primary', 'icon'=>'ok white', 'label'=>'Готово')); ?>
	</div>


<?php $this->endWidget(); ?>
