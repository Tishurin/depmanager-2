
<style type="text/css">

	th.error {width: 200px;}
	th.version {width: 60px;}
	th.project {width: 150px;}
	th.options {width: 30px;}
</style>


<?php if ($modProject->isActive): ?>
	

	<?php $modProject->check() ?>

	<?php if (!$modProject->hasErrors): ?>
		<div class='alert alert-success'>Все хорошо</div>
	<?php else: ?>
		<div class='alert alert-error'>Есть над чем подумать</div>

		<table class='table table-striped table-bordered table-hover'>

			<thead>
				<tr>
					<th>Файл</th>
					<th class='version'>Версия</th>

					<th>Проект</th>
					<th class='version'>Версия</th>

					<th class='error'>Сообщение</th>
					<th class='options'></th>

				</tr>
			</thead>

			<?php $checkPath = '' ?>
			<?php foreach ($modProject->checkErrors as $path => $sttCheckError): ?>
				

				<?php if ($checkPath != $path): ?>
					<?php $checkPath = $path ?>
					<?php $rowspan = count($sttCheckError['errors']) ?>
					<?php $flgNew = true ?>				
				<?php endif ?>

				<?php foreach ($sttCheckError['errors'] as $sttError): ?>
					<tr>

						<?php if ($flgNew): ?>
							<td rowspan='<?php echo $rowspan ?>'>
								<?php if ($sttCheckError['checkFile']->getIsLocal()): ?>
									<span class='label label-info'><?php echo $sttCheckError['checkFile']->path ?></span>
								<?php else: ?>
									<?php echo $sttCheckError['checkFile']->path ?>
								<?php endif ?>


							<td rowspan='<?php echo $rowspan ?>'>
								<?php echo CHtml::link($sttCheckError['checkFile']->version, array('/projects/file/view', 'id' => $sttCheckError['checkFile']->id))  ?></td>
							</td>
							<?php $flgNew = false ?>										
						<?php endif ?>

						<td><?php echo CHtml::link($sttError['reqFile']->project_name, array('/projects/project/view', 'id' => $sttError['reqFile']->id_project)) ?></td>
						<td>
							<?php echo CHtml::link($sttError['reqFile']->version, array('/projects/file/view', 'id' => $sttError['reqFile']->id))  ?>
						</td>

						<td><?php echo $sttError['message'] ?></td>
						<td style='text-align: center;'>
							<?php if ($sttError['allowUpdate'] == true): ?>
								<?php echo CHtml::link('<i class="icon-arrow-left"></i>', array('/projects/file/copy', 'id' => $sttError['reqFile']->id, 'id_to' => $sttCheckError['checkFile']->id), array('confirm' => 'Точно?')) ?></td>
								
							<?php endif ?>

					</tr>				
				<?php endforeach ?>
				
			<?php endforeach ?>

		</table>

	<?php endif ?>

<?php else: ?>
	<div class='alert alert-info'>Поддержка проекта прекращена</div>

<?php endif ?>


<h3 style='margin-bottom: 10px;'>Список файлов</h3>

<?php $form=$this->beginWidget('CActiveForm', array(
	'method' => 'get',
	'htmlOptions' => array('style' => 'margin-bottom: 0;'),
)); ?>
	<div class='well well-small' style='padding-right: 130px; margin-bottom: 0;'>
		<?php $this->widget('bootstrap.widgets.BootButton', array('buttonType'=>'submit', 'type' => 'primary', 'icon'=>'icon-search icon-white', 'label'=>'Искать', 'htmlOptions' => array('style' => 'float: right; text-align: left; margin-right: -120px; width: 100px;'))); ?> 
		<?php echo $form->textField($modFile, 'request', array('style' => 'margin-bottom: 0; width: 100%;')); ?> 
	</div>
<?php $this->endWidget(); ?>


<?php $this->widget('bootstrap.widgets.BootGridView', array(
	'id' => 'project-grid',
	'dataProvider' => $modFile->search(),
	'type'=>'striped bordered',
	'template' => '{items}{pager}',
	'ajaxUpdate' => false,
	'columns'=>array(
		array('name' => 'path', 'header' => 'Путь', 'type' => 'raw', 'value' => 'CHtml::link($data->path, array("/projects/file/view", "id" => $data->id))') ,
		array('name' => 'version', 'header' => 'Версия', 'htmlOptions' => array('style' => 'width: 100px;')),
		array('name' => 'change', 'header' => 'Изменен', 'type' => 'date', 'htmlOptions' => array('style' => 'width: 100px;')),

		/*
		array(
			'class'=>'bootstrap.widgets.BootButtonColumn',
		),
		*/
	),
)); ?>
