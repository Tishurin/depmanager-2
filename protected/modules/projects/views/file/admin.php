
<?php $form=$this->beginWidget('CActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
	'htmlOptions' => array('style' => 'margin-bottom: 0;'),
)); ?>
	<div class='well well-small' style='padding-right: 130px; margin-bottom: 0;'>
		<?php $this->widget('bootstrap.widgets.BootButton', array('buttonType'=>'submit', 'type' => 'primary', 'icon'=>'icon-search icon-white', 'label'=>'Искать', 'htmlOptions' => array('style' => 'float: right; text-align: left; margin-right: -120px; width: 100px;'))); ?> 
		<?php echo $form->textField($modFile, 'request', array('style' => 'margin-bottom: 0; width: 100%;')); ?> 
	</div>
<?php $this->endWidget(); ?>

<?php $this->widget('bootstrap.widgets.BootGridView', array(
	'id' => 'file-grid',
	'dataProvider' => new CActiveDataProvider($modFile, array('criteria' => array('select' => '`id`, `path`', 'order' => 'path', 'group' => 'path'), 'pagination' => array('pageSize' => 25))),
	'type'=>'striped bordered',
	'template' => '{items}{pager}',
	'ajaxUpdate' => false,
	'columns'=>array(
		array('name' => 'path', 'type' => 'raw', 'value' => 'CHtml::link($data->path, array("/projects/file/view", "id" => $data->id, "flg_no_project" => 1))'),
	),
)); ?>
