
<?php $this->widget('bootstrap.widgets.BootDetailView', array(
	'data'=>$modFile,
	'attributes'=>array(
		'path',
		'hash',
		'change:date'
	),
)); ?>


<?php highlight_string($modFile->getContent()) ?>

<h3>Этот файл в<?php if (!$flg_no_project) echo ' других' ?> проектах</h3>
<?php $arrFiles = $modFile->with('project')->findAll(array('condition' => 't.path = "' . $modFile->path . '"' . ($flg_no_project ? '' : ' AND project.id IS NOT NULL AND id_project <> ' . $modFile->id_project) , 'order' => 't.change DESC')) ?>

<?php $this->widget('bootstrap.widgets.BootGridView', array(
	'id' => 'file-grid',
	'dataProvider' => new CArrayDataProvider($arrFiles, array('pagination' => false)),
	'type'=>'striped bordered',
	'template' => '{items}{pager}',
	'ajaxUpdate' => false,
	'columns'=>array(
		array('header' => 'Проект', 'type' => 'raw', 'value' => '$data->project !== null ? CHtml::link($data->project->name, array("/projects/project/view", "id" => $data->project->id)) : ""'),
		array('name' => 'version', 'header' => 'Версия', 'htmlOptions' => array('style' => 'width: 100px;')),
		array('name' => 'change', 'header' => 'Изменен', 'type' => 'date', 'htmlOptions' => array('style' => 'width: 100px;')),

	),
)); ?>