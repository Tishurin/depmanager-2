<?php

//******************************************************************************
class File extends DActiveRecord
//******************************************************************************
{

	public $request; // Поисковый запрос

	//----------------------------------------------------------------------------
	public function attributeLabels()
	//----------------------------------------------------------------------------  
	{
		return array(
			'path' => 'Путь',
			'version' => 'Версия',
			'change' => 'Изменен',
			'hash' => 'Свертка',
		);
	}

	//----------------------------------------------------------------------------
	public function rules()
	//----------------------------------------------------------------------------	
	{
		return array(
				array('id_project, change', 'length', 'max'=>10),
				array('path, version, hash', 'length', 'max'=>255),
				// Внимание: удалите лишние атрибуты!
				array('id, id_project, path, version, change, hash, requirements, request', 'safe', 'on'=>'search'),
		);
	}

	//----------------------------------------------------------------------------
	public function relations()
	//----------------------------------------------------------------------------
	{
		// ВНИМАНИЕ: уточните имя связи
		return array(
			'project' => array(self::BELONGS_TO, 'Project', 'id_project'),
		);
	}

	//----------------------------------------------------------------------------
	public function getFullPath()
	//----------------------------------------------------------------------------
	{
		return Yii::getPathOfAlias('base') . '/' .  $this->project->path . '/' . $this->path;
	}

	//----------------------------------------------------------------------------  
	public function getContent()
	//----------------------------------------------------------------------------  
	{
		return file_get_contents($this->fullPath);
	}

	//----------------------------------------------------------------------------
	public function search()
	//----------------------------------------------------------------------------
	{
		// Внимание: удалите лишние атрибуты!

		$objCriteria = new CDbCriteria;

		// Фильтрация полей
		$objCriteria->compare('id_project', $this->id_project, false);

		// Запрос по строке
		$objCriteria->compare('path', $this->request, true, 'AND');

		$objCriteria->order = 'path';

		return new CActiveDataProvider($this, array(
			'criteria' => $objCriteria,
			'pagination' => false,
		));
	}


	public static function model($className=__CLASS__) {return parent::model($className);}
	public function tableName() {return 'tbl_files';}
}