<?php

//******************************************************************************
class Project extends DActiveRecord
//******************************************************************************
{

	public $request; // Поисковый запрос
	public $request_closed = 0; // Поисковый запрос

	public $ignore = ".git\nassets\nprotected/data\nprotected/runtime\n";

	private $_checkFiles;
	private $_externalCheckFiles;

	public $checkErrors;

	//****************************************************************************
	// AR - методы
	//****************************************************************************

	//----------------------------------------------------------------------------
	public function attributeLabels()
	//----------------------------------------------------------------------------  
	{
		return array(
			'name' => 'Наименование',
			'path' => 'Относительный путь',
			'ignore' => 'Фильтры',
			'flg_closed' => 'Закрыт',
		);
	}

	//----------------------------------------------------------------------------
	public function rules()
	//----------------------------------------------------------------------------	
	{
		return array(
				array('name, path', 'length', 'max'=>255),
				array('ignore', 'safe'),
				array('name, path', 'required'),
				array('flg_closed', 'length', 'max'=>10),

				// Внимание: удалите лишние атрибуты!
				array('request, request_closed', 'safe', 'on'=>'search'),
		);
	}

	//----------------------------------------------------------------------------
	public function relations()
	//----------------------------------------------------------------------------
	{
		// ВНИМАНИЕ: уточните имя связи
		return array(
			'files' => array(self::HAS_MANY, 'File', 'id_project', 'order' => 'path'),
		);
	}

	//----------------------------------------------------------------------------
	public function getIsClosed()
	//----------------------------------------------------------------------------
	{
		return $this->flg_closed == 1;
	}

	//----------------------------------------------------------------------------
	public function afterDelete()
	//----------------------------------------------------------------------------
	{
		parent::afterDelete();
		foreach ($this->files as $modFile) 
			$modFile->delete();
	}

	//----------------------------------------------------------------------------
	public function scopes()
	//----------------------------------------------------------------------------
	{
		return array(
			'active' => array(
					'condition' => 'flg_closed = 0',
				)
			);
	}

	//****************************************************************************
	// Собственные методы
	//****************************************************************************

	//----------------------------------------------------------------------------
	public function getIsActive()
	//----------------------------------------------------------------------------
	{
		return $this->flg_closed == 0;
	}

	//----------------------------------------------------------------------------
	public function check()
	//----------------------------------------------------------------------------
	{
		$this->refreshActiveFiles();
		$arrFiles = CheckFile::getProjectFiles($this->id);
		$arrAllFiles = CheckFile::getAllActiveFiles();

		foreach ($arrFiles as $path => $objFile) {
			$arrVersions = array();
			foreach ($arrAllFiles[$objFile->path] as $objThatFile) {
				if ($objThatFile->id_project == $objFile->id_project) continue;

				// Проверить конфликт содержимого
				$modify = $objFile->checkModify($objThatFile);
				if ($modify == CheckFile::MODIFY_THIS) {
					$this->addCheckError($objFile, $objThatFile, 'Этот файл был изменен');
					break;
				}
				elseif ($modify == CheckFile::MODIFY_THAT) 
					$this->addCheckError($objFile, $objThatFile, 'Тот файл был изменен');

				// Проверить возможность обновления
				$upd = $objFile->checkUpdate($objThatFile);
				if($upd != CheckFile::UPDATE_NONE) {
					if (!in_array($objThatFile->version, $arrVersions)) {
						switch ($upd) {

							case CheckFile::UPDATE_B:
								$this->addCheckError($objFile, $objThatFile, 'Не совместим');
								break;	

							case CheckFile::UPDATE_C:
								$this->addCheckError($objFile, $objThatFile, 'Совместим');
								break;	

							case CheckFile::UPDATE_D:
								$this->addCheckError($objFile, $objThatFile, 'Багфикс', !($objFile->getIsLocal() || $objThatFile->getIsLocal())  );
								break;
						}
										
						$arrVersions[] = $objThatFile->version;
					}
				}

			}
		}
	}

	//----------------------------------------------------------------------------
	public function addCheckError($modCheckFile, $modReqFile = null, $message = '', $allowUpdate = false)
	//----------------------------------------------------------------------------
	{
		if (!isset($this->checkErrors[$modCheckFile->path]))
			$this->checkErrors[$modCheckFile->path] = array('checkFile' => $modCheckFile, 'errors' => array());

		$this->checkErrors[$modCheckFile->path]['errors'][] = array('reqFile' => $modReqFile, 'message' => $message, 'allowUpdate' => $allowUpdate);
	}

	//----------------------------------------------------------------------------
	public function getHasErrors()
	//----------------------------------------------------------------------------
	{
		return count($this->checkErrors) > 0;
	}

	//----------------------------------------------------------------------------
	public function getIgnore()
	//----------------------------------------------------------------------------
	{
		if (!($arrRet = explode("\n", $this->ignore)) || $this->ignore == '');
		foreach ($arrRet as $k => $v) $arrRet[$k] = trim($v);
		return $arrRet;
	}

	//----------------------------------------------------------------------------
	public function refreshActiveFiles()
	//----------------------------------------------------------------------------
	{
		$dr = Yii::app()->db->createCommand()
			->select('tbl_files.*, tbl_projects.path as project_path')
			->from('tbl_files')
			->join('tbl_projects', 'id_project = tbl_projects.id')
			->where('tbl_projects.flg_closed = 0 ')
			->order('path')
			->query();

		$arrChangedFiles = array();
		foreach ($dr as $sttFile) {
			$fileAbsPath = Yii::getPathOfAlias('base') . '/' .  $sttFile['project_path'] . '/' . $sttFile['path'];

			if ($sttFile['change'] != filemtime($fileAbsPath)) {
				$hash = md5_file($fileAbsPath);
				if ($sttFile['hash'] != $hash) {
					$prsFile = $this->parseText(file_get_contents($fileAbsPath));
					$arrChangedFiles[$sttFile['id']] = array(
						'hash' => $hash,
						'version' => $prsFile['version'],
						'change' => filemtime($fileAbsPath)); 
				}
			}
		}

		if (count($arrChangedFiles) > 0) {
			$arrModFiles = File::model()->findAllByPk(array_keys($arrChangedFiles));
			foreach ($arrModFiles as $modFile) {
				$modFile->attributes = $arrChangedFiles[$modFile->id];
				$modFile->save();
			}
		}


	}

	//----------------------------------------------------------------------------
	public function refreshFileIndex()
	//----------------------------------------------------------------------------
	// Обновить индекс проходом по файлам
	{
		File::model()->deleteAllByAttributes(array('id_project' => $this->id));

		// Зарегистрировать файлы
		$this->refreshFileIndexR();
	}

	//----------------------------------------------------------------------------
	public function refreshFileIndexR($path = '')
	//----------------------------------------------------------------------------
	{
		$foldAbsPath = Yii::getPathOfAlias('base') . '/' .  $this->path . '/' . $path;
		
		$hdlDir = opendir($foldAbsPath);
		$arrIgnore = $this->getIgnore();
		while(($strFile = readdir($hdlDir)) !== false) 
		{
			if ($strFile == '.' || $strFile == '..') continue;

			$fileProjPath = $path . $strFile;
			$fileAbsPath = $foldAbsPath . $strFile;

			if (is_dir($fileAbsPath) && !in_array($fileProjPath, $arrIgnore)) $this->refreshFileIndexR($fileProjPath . '/');
			elseif (is_file($fileAbsPath)) {

				// Зарегистрировать файлы
				if ($sttFile = $this->parseText(file_get_contents($fileAbsPath))) {
					$modFile = new File;
					$modFile->id_project = $this->id;
					$modFile->path = $fileProjPath;
					$modFile->version = $sttFile['version'];
					$modFile->hash = md5_file($fileAbsPath);
					$modFile->change = filemtime ($fileAbsPath);
					$modFile->save();		
				}
			}
		}
		
	}

	//----------------------------------------------------------------------------
	public function parseText($strText)
	//----------------------------------------------------------------------------
	{
		$arrRet = false;

		if ($posVer = (strpos($strText, '# ver: ')) ) {
			
			$posVerE = $posVer + strlen('# ver: ');
			$posE = strpos($strText, "\n", $posVerE);

			$arrRet = array('version' => '');
			$arrRet['version'] = trim(substr($strText, $posVerE, $posE - $posVerE));		
		}

		return $arrRet;
	}

	//****************************************************************************
	// Поиск
	//****************************************************************************

	//----------------------------------------------------------------------------
	public function search()
	//----------------------------------------------------------------------------
	{
		// Внимание: удалите лишние атрибуты!

		$objCriteria = new CDbCriteria;

		if ($this->request_closed == 0)
			$objCriteria->compare('flg_closed', 0, false);

		$objCriteria->compare('name', $this->request, true, 'OR');


		$objCriteria->order = 'name';

		return new CActiveDataProvider($this, array(
			'criteria' => $objCriteria,
			'sort' => false,
		));
	}


	public static function model($className=__CLASS__) {return parent::model($className);}
	public function tableName() {return 'tbl_projects';}
}