<?php


class CheckFile
{
	// Свойства
	public $id;
	public $id_project;
	public $project_name;
	public $project_path;
	public $path;
	public $version;
	public $change;
	public $hash;

	private $_parsedVersion;

	const MODIFY_OK = 0;
	const MODIFY_THIS = 1;
	const MODIFY_THAT = 2;

	//----------------------------------------------------------------------------  
	public static function getProjectFiles($id_project)
	//----------------------------------------------------------------------------  
	{
		$arrRet = array();

		$dr = Yii::app()->db->createCommand()
			->select('tbl_files.*, tbl_projects.name as project_name, tbl_projects.path as project_path')
			->from('tbl_files')
			->join('tbl_projects', 'id_project = tbl_projects.id')
			->where('id_project = ' . $id_project)
			->order('path, project_name')
			->query();

		foreach ($dr as $sttFile)
			$arrRet[$sttFile['path']] = self::build($sttFile);

		return $arrRet;		
	}

	//----------------------------------------------------------------------------  
	public static function getAllActiveFiles()
	//----------------------------------------------------------------------------  
	{
		$arrRet = array();

		$dr = Yii::app()->db->createCommand()
			->select('tbl_files.*, tbl_projects.name as project_name, tbl_projects.path as project_path')
			->from('tbl_files')
			->join('tbl_projects', 'id_project = tbl_projects.id')
			->where('tbl_projects.flg_closed = 0 ')
			->order('path, project_name')
			->query();

		foreach ($dr as $sttFile)
			$arrRet[$sttFile['path']][] = self::build($sttFile);

		return $arrRet;				
	}

	//----------------------------------------------------------------------------  
	public static function build($sttFile)
	//----------------------------------------------------------------------------  
	{
		$modCheckFile = new CheckFile();
		$modCheckFile->id = $sttFile['id'];
		$modCheckFile->id_project = $sttFile['id_project'];
		$modCheckFile->project_name = $sttFile['project_name'];
		$modCheckFile->project_path = $sttFile['project_path'];
		$modCheckFile->path = $sttFile['path'];
		$modCheckFile->version = $sttFile['version'];
		$modCheckFile->change = $sttFile['change'];
		$modCheckFile->hash = $sttFile['hash'];
		return $modCheckFile;		
	}

	//----------------------------------------------------------------------------  
	public function checkModify($objFile)
	//----------------------------------------------------------------------------  
	// Версии совпадают, а файлы разные
	{
		if ($this->version != $objFile->version || $this->getIsLocal() || $this->hash == $objFile->hash)
			return self::MODIFY_OK;

		if ($this->change > $objFile->change) return self::MODIFY_THIS;
		else return self::MODIFY_THAT;
	}

	//----------------------------------------------------------------------------  
	public function getParsedVersion()
	//----------------------------------------------------------------------------  
	{
		if ($this->_parsedVersion == null)
			$this->_parsedVersion = $this->parseVersion($this->version);	

		return $this->_parsedVersion;
	}

	//----------------------------------------------------------------------------  
	public function getIsLocal()
	//----------------------------------------------------------------------------  
	{
		$ver = $this->getParsedVersion();
		if ($ver['pb'] . $ver['pc'] != '') return true;
		else return false;  
	}



	//----------------------------------------------------------------------------  
	public function parseVersion($strVersion)
	//----------------------------------------------------------------------------  
	{
		$stt = array_fill_keys(array('a', 'pb', 'b', 'pc', 'c', 'd'), '');

		$arr = explode('.', $strVersion);
		$c = 2;
		$d = 3;

		$stt['a'] = $arr[0];

		// pb и b
		if (is_numeric($arr[1])) $stt['b'] = $arr[1];
		else {
			$stt['pb'] = $arr[1];
			$stt['b'] = $arr[2];
			$d = 4;
			$c = 3;
		}

		// pc и c
		if (isset($arr[$c])) {
			if (is_numeric($arr[$c])) $stt['c'] = $arr[$c];
			else {
				$stt['pc'] = $arr[$c];
				if (isset($arr[$c + 1])) $stt['c'] = $arr[$c + 1];
				$d = 4;
			}	
		}
	
		// d
		if (isset($arr[$d])) $stt['d'] = $arr[$d];	

		return $stt;
	}

	const UPDATE_NONE = 0;
	const UPDATE_B = 1;
	const UPDATE_C = 2;
	const UPDATE_D = 3;

	//----------------------------------------------------------------------------  
	public function checkUpdate($objFile)
	//----------------------------------------------------------------------------  
	{
		$v1 = $this->getParsedVersion();
		$v2 = $this->parseVersion($objFile->version);		

		// Обновляются только файлы сквозного контроля
		if ($v1['pb'] . $v2['pb'] == '') {
			
			// Обновляются только файлы с одинаковым a
			if ($v1['a'] == $v2['a']) {

				if ($v1['b'] < $v2['b']) return self::UPDATE_B;
				elseif ($v1['b'] == $v2['b']) {

					// Обнуляем младшие индексы
					if ($v1['d'] == '') $v1['d'] = 0;
					if ($v2['d'] == '') $v2['d'] = 0;	

					if ($v1['c'] < $v2['c']) return self::UPDATE_C;
					elseif ($v1['c'] == $v2['c']) {

						if ($v1['d'] < $v2['d']) return self::UPDATE_D;

					}
				}
			}
		}

		return self::UPDATE_NONE;	
	}
}