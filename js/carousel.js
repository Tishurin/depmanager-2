$(function(){

		$.fn.main_carousel = function(options) {		 
		 
			var settings = {
				  speed:500,
				  delay:0
 				};
				
			if (options) $.extend(settings, options); 
			

	
			$.each($(this),function(){
				
					var width_img; 
					var this_obj  = $(this);
					var list_links = this_obj.find(".remove_block");
					var marker = 1; 
					var id_setInterval;
					var num =0;
					
																	 
					$("<a href=\"#\" class=\"left\"></a><a href=\"#\" class=\"right\"></a>").appendTo(this_obj);

					var btns="";
					for(i=1; i<this_obj.find(".carousel_cell").length+1; i++){
					    this_obj.find(".carousel_cell").eq(i-1).attr("rel",i);
					    i==1 ? btns +="<a href=\"#\" class=\"current\" rel=\""+i+"\"></a>":btns +="<a href=\"#\" rel=\""+i+"\"></a>";
					}			

		 	        $("<div class=\"switchs\">"+btns+"</div>").appendTo(this_obj);
					
 				

					function scrollImgRight() {
						marker = 0;
						width_img = list_links.find(".carousel_cell:last").width(); 				
 
						list_links.find(".carousel_cell:last").css("margin-left", -width_img);
						list_links.find(".carousel_cell:last").prependTo($(".remove_block"));
		 
						list_links.find(".carousel_cell:first").animate({
							'margin-left': 0
						}, settings.speed, function () {

							marker = 1;
							
							
							index_num = this_obj.find(".switchs a.current").index();
							this_obj.find(".switchs a").removeClass("current");
                            index_num-1 < 0 ? this_obj.find(".switchs a").eq(this_obj.find(".switchs a").length-1).addClass("current") : this_obj.find(".switchs a").eq(index_num-1).addClass("current");

							

						});
					}

					
					function scrollImgLeft() {
            marker = 0;
						width_img = list_links.find(".carousel_cell:first").width(); 				

						list_links.find(".carousel_cell:first").animate({
							'margin-left': -width_img
						}, settings.speed, function () {

							list_links.find(".carousel_cell:first").appendTo($(".remove_block"));
							list_links.find(".carousel_cell:last").css("margin-left", "0");

							marker = 1;
							
							index_num = this_obj.find(".switchs a.current").index();
							this_obj.find(".switchs a").removeClass("current");
                            index_num+1 < this_obj.find(".switchs a").length ? this_obj.find(".switchs a").eq(index_num+1).addClass("current"): this_obj.find(".switchs a").eq(0).addClass("current");
						});
					} 


					
					function scrollImg() {
					
						marker = 0;
						
						width_img = list_links.find(".carousel_cell:first").width(); 				

						list_links.animate({
							'margin-left': -width_img*num
						}, settings.speed, function () {
 
							
                            for( var i=1;i<=num; i++){
							   list_links.find(".carousel_cell:first").appendTo($(".remove_block"));
							   list_links.css("margin-left",-width_img*(num-i)+"px") 							
							}  
							marker = 1;

						});
					} 
					
					
					this_obj.find(".right").bind('click',function () {

					    scrollImgLeft(); 
						return false;

					});

					this_obj.find(".left").bind('click',function () {

						scrollImgRight();
						return false;
						
					});
					
					this_obj.find(".switchs a").bind('click',function () {
					
 					    num2 = $(this).attr("rel");
						num = list_links.find(".carousel_cell[rel="+num2+"]").index();
 						
   					    this_obj.find(".switchs a").removeClass("current");
                        $(this).addClass("current");
                        
						scrollImg();
						return false;
						
					});					
 	
 
					
		 
			})
			
		};
  		   
        $(".carousel").main_carousel();
		   
});